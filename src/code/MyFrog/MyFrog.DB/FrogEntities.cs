﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyFrog.DB.Models;
using System.Data.Entity;
using System;

namespace MyFrog.DB
{
    public class FrogEntities: IdentityDbContext<ApplicationUser>, IDisposable
    {
        public FrogEntities() : base("name=AppConnectionString")
        {
            Database.SetInitializer<FrogEntities>(new MyFrogInitializer<FrogEntities>());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Pond> Ponds { get; set; }

    }
    public class MyFrogInitializer<T> : DropCreateDatabaseIfModelChanges<FrogEntities>
    {
        protected override void Seed(FrogEntities context)
        {
            if (!context.Roles.AnyAsync(r => r.Name == "Admin1").Result)
            {
                var s = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(s);
                var role = new IdentityRole { Name = "Admin1" };
                manager.Create(role);
            }
            //if (!context.Roles.AnyAsync(r => r.Name == "Public").Result)
            //{
            //    var s = new RoleStore<IdentityRole>(context);
            //    var manager = new RoleManager<IdentityRole>(s);
            //    var role = new IdentityRole { Name = "Public" };
            //    manager.Create(role);
            //}
            base.Seed(context);
        }

       /* protected override void Seed(FrogEntities context)
        {
            if (context.Roles.FirstOrDefaultAsync(r => r.Name == "Admin") == null)
            {
                var s = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(s);
                var role = new IdentityRole { Name = "Admin" };
                manager.Create(role);
            }

            //if (context.Roles.FirstOrDefaultAsync(r => r.Name == "Public") == null)
            //{
            //    var s = new RoleStore<IdentityRole>(context);
            //    var manager = new RoleManager<IdentityRole>(s);
            //    var role = new IdentityRole { Name = "Public" };
            //    manager.Create(role);
            //}

            //if (context.Users.FirstOrDefaultAsync(u => u.UserName == "salik.chughtai") == null)
            //{
            //    var obj = new UserStore<ApplicationUser>(context);
            //    var manager = new UserManager<ApplicationUser>(obj);
            //    var user = new ApplicationUser { UserName = "salik.chughtai" };

            //    manager.Create(user, "Test123");
            //    manager.AddToRole(user.Id, "Public");
            //}
            base.Seed(context);
        }*/
    }
}
