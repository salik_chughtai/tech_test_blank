namespace MyFrog.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MyFrog.Models.FrogEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MyFrog.Models.FrogEntities context)
        {
            if (context.Roles.FirstOrDefaultAsync(r => r.Name == "Admin") == null)
            {
                var s = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(s);
                var role = new IdentityRole { Name = "Admin" };
                manager.Create(role);
            }

            if (context.Roles.FirstOrDefaultAsync(r => r.Name == "Public") == null)
            {
                var s = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(s);
                var role = new IdentityRole { Name = "Public" };
                manager.Create(role);
            }

            if (context.Users.FirstOrDefaultAsync(u => u.UserName == "salik.chughtai") == null)
            {
                var obj = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(obj);
                var user = new ApplicationUser { UserName = "salik.chughtai", FirstName = "Salik", LastName = "Chughtai" };

                manager.Create(user, "Test@123");
                manager.AddToRole(user.Id, "Public");
            }
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
