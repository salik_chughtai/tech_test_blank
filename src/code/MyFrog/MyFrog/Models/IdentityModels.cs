﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace MyFrog.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class FrogEntities : IdentityDbContext<ApplicationUser>
    {
        public FrogEntities()
            : base("AppConnectionString", throwIfV1Schema: false)
        {
        }

        public static FrogEntities Create()
        {
            return new FrogEntities();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Pond> Ponds { get; set; }

    }
    //public class MyFrogInitializer<T> : DropCreateDatabaseIfModelChanges<FrogEntities>
    //{
    //    /* protected override void Seed(FrogEntities context)
    //     {
    //          if (!context.Roles.AnyAsync(r => r.Name == "Admin1").Result)
    //          {
    //              var s = new RoleStore<IdentityRole>(context);
    //              var manager = new RoleManager<IdentityRole>(s);
    //              var role = new IdentityRole { Name = "Admin1" };
    //              manager.Create(role);
    //          }
    //          //if (!context.Roles.AnyAsync(r => r.Name == "Public").Result)
    //          //{
    //          //    var s = new RoleStore<IdentityRole>(context);
    //          //    var manager = new RoleManager<IdentityRole>(s);
    //          //    var role = new IdentityRole { Name = "Public" };
    //          //    manager.Create(role);
    //          //}
    //          base.Seed(context);
    //      }*/

    //    protected override void Seed(FrogEntities context)
    //    {
    //        if (context.Roles.FirstOrDefaultAsync(r => r.Name == "Admin") == null)
    //        {
    //            var s = new RoleStore<IdentityRole>(context);
    //            var manager = new RoleManager<IdentityRole>(s);
    //            var role = new IdentityRole { Name = "Admin" };
    //            manager.Create(role);
    //        }

    //        if (context.Roles.FirstOrDefaultAsync(r => r.Name == "Public") == null)
    //        {
    //            var s = new RoleStore<IdentityRole>(context);
    //            var manager = new RoleManager<IdentityRole>(s);
    //            var role = new IdentityRole { Name = "Public" };
    //            manager.Create(role);
    //        }

    //        if (context.Users.FirstOrDefaultAsync(u => u.UserName == "salik.chughtai") == null)
    //        {
    //            var obj = new UserStore<ApplicationUser>(context);
    //            var manager = new UserManager<ApplicationUser>(obj);
    //            var user = new ApplicationUser { UserName = "salik.chughtai", FirstName = "Salik", LastName = "Chughtai" };

    //            manager.Create(user, "Test@123");
    //            manager.AddToRole(user.Id, "Public");
    //        }
    //        base.Seed(context);
    //    }
    //}
}