﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyFrog.Startup))]
namespace MyFrog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
