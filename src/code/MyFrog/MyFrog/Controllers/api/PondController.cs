﻿using MyFrog.Logic.Services;
using MyFrog.Model;
using MyFrog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyFrog.Controllers.api
{
    public class PondController : ApiController
    {
        public string Test() { return "OK"; }
        [HttpGet]
        public PondModel TestPond()
        {
            using (var context = new Models.FrogEntities())
            {

                context.Ponds.Add(
                new Pond()
                {
                    Name = "Test"
                });
                context.SaveChanges();
            }
            return new PondModel() { Name = "Test" };
        }

    }
}
