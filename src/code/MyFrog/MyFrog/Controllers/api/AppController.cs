﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyFrog.Logic.Services;
using MyFrog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyFrog.Controllers.api
{
    public class AppController : ApiController
    {
        [HttpGet]
        public bool init()
        {
            return Init();
        }
        public static bool Init()
        {
            using (var context = new FrogEntities())
            {
                if (!context.Roles.Any(r => r.Name == "Admin"))
                {
                    var s = new RoleStore<IdentityRole>(context);
                    var manager = new RoleManager<IdentityRole>(s);
                    var role = new IdentityRole { Name = "Admin" };
                    manager.Create(role);
                }
                if (!context.Roles.Any(r => r.Name == "Public"))
                {
                    var s = new RoleStore<IdentityRole>(context);
                    var manager = new RoleManager<IdentityRole>(s);
                    var role = new IdentityRole { Name = "Public" };
                    manager.Create(role);
                }
                if (!context.Users.Any(u => u.UserName == "salik.chughtai"))
                {
                    var obj = new UserStore<ApplicationUser>(context);
                    var manager = new UserManager<ApplicationUser>(obj);
                    var user = new ApplicationUser { UserName = "salik.chughtai", LastName = "Chughtai", FirstName = "Salik" };

                    manager.Create(user, "Test123");
                    manager.AddToRole(user.Id, "Public");
                }
            }
            return false;
        }
    }
}
