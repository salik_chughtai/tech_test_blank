﻿import { Component } from '@angular/core';
@Component({
  selector: 'my-app',
  template: `<h1>Hello {{a}}</h1>`,
})
export class AppComponent  { a = 'Test!'; }