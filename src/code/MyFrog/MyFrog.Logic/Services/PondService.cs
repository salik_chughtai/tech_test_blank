﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyFrog.DB;
using MyFrog.DB.Models;
using MyFrog.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFrog.Logic.Services
{
    public class PondService
    {
        public static PondModel GetTestPond()
        {

            /*using (var context = new FrogEntities())
            {
                
                context.Ponds.Add(
                new Pond()
                {
                    Name = "Test"
                });
                context.SaveChanges();
            }*/
            return new PondModel() { Name = "Test" };
        }
    }
}
